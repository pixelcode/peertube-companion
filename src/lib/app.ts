import * as Settings from './settings'
import Browser from 'webextension-polyfill'
// import { BROWSER_MESSAGE, toggleIcon, isEnabled } from './utils'
import { BROWSER_MESSAGE, isEnabled } from './utils'

export const toggle = async () => {
  await isEnabled() ? await disable() : await enable()
}

export const disable = async () => {
  await Settings.set('app.isEnabled', false)
  await Browser.runtime.sendMessage(BROWSER_MESSAGE.APP_DISABLE)
  // await toggleIcon()
}

export const enable = async () => {
  await Settings.set('app.isEnabled', true)

  await Browser.runtime.sendMessage(BROWSER_MESSAGE.APP_ENABLE)
  // await toggleIcon()
}
