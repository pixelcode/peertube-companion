import * as Settings from "../settings"
import { fetchApi, ping, sortByPing } from "../utils"
import * as Cache from "../cache"
import { isValidHostToRedirect } from "../redirection"
import { URL_TYPE } from "../backend"

export const getVideoTitle = async (url: URL) => {
  try {
    const videoDetails = await getVideoDetails(url)

    return videoDetails ? videoDetails.title : false

  } catch (e: any) {
    console.error(e.message)
  }
}

export const getVideoDetails = async (url: URL) => {
  try {
    if (await isValidHostToRedirect(url)) {
      const id = url.searchParams.get("v")

      if (id) {
        const r = await Cache.getOrSave(`invidious-getVideoDetails-${encodeURIComponent(id)}`,
          async () => 
            fetchApi(`${await getInstanceApiUrl()}/v1/videos/${id}`),
          await Settings.get('cache.ttl.invidious.getVideoDetails')
        )

        return "type" in r ? r : false
      }
    }

    return false
  } catch (e: any) {
    console.error(e.message)
  }
}

export const pickInstance = async (random = false, bypassCache = false) => {
  let result = async () => {}

  if (random) {
    let instances = await getInstancesPing()

    if (instances) {
      const filteredInstances = instances.filter(i => { // Pick between fastest instances
        return i.ping < 300
      })

      instances = filteredInstances.length > 0 ? filteredInstances : instances // If no instance is fast, pick between all of them

      result = async () => await instances?.[Math.floor(Math.random() * instances?.length)]
    }
  } else {
    result = async () => getFastestInstance()
  }

  return bypassCache
    ? result()
    : Cache.getOrSave("invidious-pickInstance",
      async () => await result(),
      await Settings.get('cache.ttl.invidious.pickInstance')
    )
}

export const getFastestInstance = async () => {
  return (await getInstancesPing())?.sort(sortByPing)[0]
}

export const getIndexedInstances = async (filter = true) => {
  return Cache.getOrSave(`invidious-getIndexedInstances-${encodeURIComponent(await Settings.get('api.invidious.indexUrl'))}-${filter}`,
    async () => {
      const r = await fetchApi(`${await Settings.get('api.invidious.indexUrl')}/instances.json?pretty=1&sort_by=type,users`)

      if (r?.length > 0) {
        return filter
          ? (r).filter((i: any[]) => {
            const baseCondition = i[1].type !== "onion" && i[1].api

            // Work only with non-tor instances, having an API & having 100% upstream today
            return i[1].monitor
              ? baseCondition && i[1].monitor.dailyRatios[0].ratio === "100.00"
              : baseCondition
          })
          : r
      }

      return null
    },
    await Settings.get('cache.ttl.invidious.getIndexedInstances')
  )
}

export const getInstancesPing = async () => {
  const indexedInstances = await getIndexedInstances()

  if (indexedInstances) {
    return (await Promise.all(indexedInstances.map(async (instance: any) => {
      const u = new URL(`${instance[1].uri}`)

      return { url: instance[1].uri, ping: await ping(u, 1500, true), type: URL_TYPE.INVIDIOUS }

    }))).filter(i => i.ping > 0)
  }

  return null
}

const getInstanceApiUrl = async () => {
  const url = await Settings.get('api.invidious.preferredInstance') || (await pickInstance()).url

  return `${url}/api`
} 
