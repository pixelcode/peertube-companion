// import { config } from "../../config"
// import { fetchApi, ping } from "../utils"
// import * as Cache from "../cache"
// import { isValidHostToRedirect } from "../redirect"

// export const getVideoTitle = async (url: URL, apiInstance: URL) => {
//   try {
//     const videoDetails = await getVideoDetails(url, apiInstance)

//     if (videoDetails) {
//       return videoDetails.title
//     }
//   } catch (e: any) {
//     console.error(e.message)
//   }
// }

// export const getVideoDetails = async (url: URL, apiInstance: URL) => {
//   try {
//     if (await isValidHostToRedirect(url)) {
//       const id = url.searchParams.get("v")

//       const r = await Cache.getOrSave(`piped-getVideoDetails-${encodeURIComponent(id)}`,
//         async () => fetchApi(`${apiInstance.origin}/streams/${id}`))

//       return "error" in r ? false : r
//     }

//     return false
//   } catch (e: any) {
//     console.error(e.message)
//   }
// }

// export const pickInstance = async (random = false, bypassCache = false) => {
//   let result = async () => {}

//   if (random) {
//     let instances = await getInstancesPing()

//     const filteredInstances = instances.filter(i => { // Pick between fastest instances
//       return i.ping < 150
//     })

//     instances = filteredInstances.length > 0 ? filteredInstances : instances // If no instance is fast, pick between all of them

//     result = async () => await instances[Math.floor(Math.random() * instances.length)]
//   } else {
//     result = async () => getFastestInstance()
//   }

//   return bypassCache
//     ? result()
//     : Cache.getOrSave("piped-pickInstance",
//       async () => await result())
// }

// export const getFastestInstance = async () => {
//   return (await getInstancesPing()).filter(i => i.ping > 0).sort((a, b) => { // ASC sort, ignore timeout results
//     if (a.ping > b.ping) return 1
//     if (a.ping < b.ping) return -1
//     return 0
//   })[0]
// }

// export const getInstancesPing = async () => {
//   return Promise.all(config.piped.apiUrls.map(async (url) => {
//     const u = new URL(`${url}trending`) // Use trending endpoint to avoid 404 error

//     return { url, ping: await ping(u, 3000, true, true) }

//   }))
// }
