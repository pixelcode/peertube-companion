import { fetchApi, ping } from "../utils"
import * as Cache from "../cache"
import * as Settings from "../settings"
import { URL_TYPE } from "../backend"

export interface APIVideosArray {
  total: number
  data: any[]
}

export enum MATCHING_RULES {
  EXACT_TITLE,
  ORIGINAL_INSTANCE,
  NO_RULE
}

/**
 * It takes a string, encodes it, and then fetches the result from the indexUrl
 * @param {string} title - The title of the video you want to search for.
 * @returns An array of videos
 */
export const searchByTitle = async (title: string) => {
  try {
    return Cache.getOrSave(`searchByTitle-${encodeURIComponent(title)}`,
      async () => 
        fetchApi(`${await getApiUrl(true)}/v1/search/videos?search=${encodeURIComponent(title)}`),
      await Settings.get('cache.ttl.peertube.searchByTitle')
    )
  } catch (e: any) {
    console.error(e.message)
    return false
  }
}

export const getFirstAvailable = async (videos: any[]) => {
  for (const v of videos) {
    console.log(v)
    if (await ping(new URL(v.url), await Settings.get('cache.ttl.peertube.getPerfectMatch')) > 0) {
      return v
    }
  }

  return false
}

export const getVideoMatchingRules = async (rules: { rule: MATCHING_RULES, args?: any }) => {
  try {
    let videos = []

    switch(rules.rule) {
      case MATCHING_RULES.EXACT_TITLE:
        // TODO: Choisir l'API en fonction des préférences
        // Get video matching exactly or return the first video from the array (Sepia consider it the best matching)
        videos = (await searchByTitle(rules.args.title)).data.filter((v: any) => {
          return v.name.toLowerCase() === rules.args.title.toLowerCase()
        })
      
        break
      case MATCHING_RULES.ORIGINAL_INSTANCE: {
        if (!await Settings.get('redirection.toOriginalInstance')) {
          return false
        }

        const videoDetails = await getVideoDetails(rules.args.url)

        if (videoDetails.url !== rules.args.url.href) {
          videos.push(videoDetails)
        }


        break
      }

      case MATCHING_RULES.NO_RULE: 
        return false
        break
      default:
        console.error('Rule unknown: ' + rules.rule)

        return false
    }

    if (videos.length > 0) {
      // Be sure the video is available
      return getFirstAvailable(videos)
    }

    return false
  } catch (e: any) {
    console.error(e.message)
    return false
  }
}

/**
 * It takes a URL, extracts the UUID from the URL, and then uses that UUID to fetch the video details
 * from the API
 * @param {URL} url - The URL of the video you want to get the details of.
 * @returns The video details
 */
export const getVideoDetails = async (url: URL) => {
  try {
    url = handleLazyLoad(url)

    const uuid = url.pathname.split("/")[url.pathname.split("/").length - 1]

    if (uuid) {
      return Cache.getOrSave(`peertube-getVideoDetails-${encodeURIComponent(url.href)}`,
        async () => await fetchApi(`${url.origin}/api/v1/videos/${uuid}`),
        await Settings.get('cache.ttl.peertube.getVideosDetails')
      )
    }

    return false

  } catch (e: any) {
    console.error(e.message)
    return false
  }
}

export const getIndexConfig = async (url: URL) => {
  try {
    return Cache.getOrSave(`peertube-getIndexConfig-${encodeURIComponent(url.host)}`,
      async () => fetchApi(`${await getApiUrl()}/v1/config`),
      await Settings.get('cache.ttl.peertube.getIndexConfig')
    )
  } catch (e: any) {
    console.error(e.message)
    return false
  }
}

export const getIndexedInstances = async () => {
  try {
    const ptSettings = (await Settings.load()).api.peertube

    return Cache.getOrSave(`peertube-getIndexedInstances-${encodeURIComponent(ptSettings.indexUrl)}`,
      async () => (await fetchApi(`${ptSettings.indexUrl}/api/v1/instances/hosts?count=9999999`))?.data.map((o: any) => {
        return {
          url: o.host,
          ping: 0,
          type: URL_TYPE.PEERTUBE
        }
      }),
      await Settings.get('cache.ttl.peertube.getIndexedInstances')
    )
  } catch (e: any) {
    console.error(e.message)
    return false
  }
}

/**
 * It fetches the PeerTube instance's config
 * @param {URL} url - The URL of the PeerTube instance
 * @returns The config of the instance
 */
export const getInstanceConfig = async (url: URL) => {
  try {
    return Cache.getOrSave(`peertube-getInstanceConfig-${encodeURIComponent(url.href)}`,
      async () => fetchApi(`${await getApiUrl()}/v1/config`),
      await Settings.get('cache.ttl.peertube.getInstanceConfig')
    )
  } catch (e: any) {
    console.error(e.message)
    return false
  }
}

const getApiUrl = async (isSearch = false) => {
  try {
    const s = await Settings.load()
    return `${isSearch ? s.api.peertube.searchUrl : s.api.peertube.indexUrl}/api`
  } catch (e: any) {
    console.error(e.message)
    return false
  }
}

export const handleLazyLoad = (url: URL) => {
  return url.pathname.includes('lazy-load-video;') ? new URL(decodeURIComponent(url.pathname.split('url=')[1])) : url
}
