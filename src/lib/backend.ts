import * as Settings from "./settings"
import * as Invidious from "./api/invidious"
import { BROWSER_MESSAGE, info, isEnabled } from "./utils"
import Browser from "webextension-polyfill"

export enum URL_TYPE {
  INVIDIOUS,
  PIPED,
  PEERTUBE,
  YOUTUBE
}

export type Instance = {
  url: "",
  ping: number,
  type?: URL_TYPE
}

export const pickInstance = async () => {
  try {
    const settings = await Settings.load()
  
    switch(settings.app.preferredApi) {
      case "invidious":
      default:
        settings.api.invidious.preferredInstance = settings.api.invidious.preferredInstance || (await Invidious.pickInstance()).url
  
        await Settings.save(settings)
        Browser.runtime.sendMessage(BROWSER_MESSAGE.INSTANCE_SELECTED_UPDATE)
        break
    }
  } catch (e: any) {
    console.error(e.message)
  }
}
export const getInstancesPing = async () => {
  try {
    let instances: Instance[] = []
  
    switch(await Settings.get('app.preferredApi')) {
      case "invidious":
      default:
        if (await isEnabled("api.invidious")) {
          instances = await Invidious.getInstancesPing() ?? instances
        }
  
        break
    }
  
    return instances
  } catch(e: any) {
    console.error(e.message)
  }
}

export const getIndexedInstances = async () => {
  try {  
    switch(await Settings.get('app.preferredApi')) {
      case "invidious":
      default:
        if (await isEnabled("api.invidious")) {
          return (await Invidious.getIndexedInstances())?.map((o: any) => 
          {
            return {
              url: o[1].uri,
              ping: 0,
              type: URL_TYPE.INVIDIOUS
            } as Instance
          } )
    
        }
        break
    }
  
    return false
  } catch(e: any) {
    console.error(e.message)
  }
}

export const getPreferredInstance = async () => {
  try {
    const settings = await Settings.load()
  
    switch(settings.app.preferredApi) {
      case "invidious":
      default:
        return await isEnabled("api.invidious") ? settings.api.invidious.preferredInstance || null : null
    }
  } catch (e: any) {
    console.error(e.message)
  }
}

export const savePreferredInstance = async (url: URL) => {
  try {
    const settings = await Settings.load()
  
    switch(settings.app.preferredApi) {
      case "invidious":
      default:
        settings.api.invidious.preferredInstance = url.origin
  
        if (await isEnabled("api.invidious")) {
          await Settings.save(settings)
  
          info(`Saved instance ${url.origin} as the preferred one.`)
        } 
  
        break
    }
  } catch(e: any) {
    console.error(e.message)
  }  
}
