import { config } from "../config"
import Browser from "webextension-polyfill"
import { info } from "./utils"
import defaults from "lodash.defaults"
import { default as lodashGet } from "lodash.get"
import { default as lodashSet } from "lodash.set"

export const clear = async () => {
  try {
    return await Browser.storage.local.remove("settings")
  } catch (e: any) {
    console.error(e.message)
  }
}

export const get = async (key: string) => {
  try {
    return lodashGet(await load(), key)
  } catch (e:any) {
    console.error(e.message)
  }
}

const initSettings = async () => {
  try {
    await save(config)
    return config
  } catch (e: any) {
    console.error(e.message)
  }
}

export const load = async () => {
  try {
    const s = await Browser.storage.local.get("settings")

    if (s.settings) {
      return defaults(s.settings, config)
    } else {
      info("Settings not found. Settings will be initialized.")

      return await initSettings()
    }
  } catch (e: any) {
    console.error(e.message)
  }
}

export const resetSettings = async () => {
  try {
    await clear()
    await initSettings()
  } catch (e: any) {
    console.error(e.message)
  }
}

export const save = async (settings: any) => {
  try {
    await Browser.storage.local.set({ settings })
    info("Settings has been saved")
  } catch (e: any) {
    console.error(e.message)
  }
}

export const set = async (key: string, value: any) => {
  try {
    const settings = await load()

    lodashSet(settings, key, value)

    await Browser.storage.local.set({ settings })
    info("Settings has been saved")
  } catch (e: any) {
    console.error(e.message)
  }
}
