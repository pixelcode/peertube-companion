import Browser from "webextension-polyfill"
import { info } from "./utils"

interface CacheItem {
  data: any
  timestamp: number
  ttl: number
}

export const clear = async () => {
  try {
    info("Cache has been cleared")
    await Browser.storage.local.clear()
  } catch (e: any) {
    console.error(e.message)
  }
}

export const get = async (name: string) => {
  try {
    const cache = await load()

    if (cache?.[name]) {
      if (isValid(cache[name])) {
        return cache[name]
      } else {
        void invalidate(name)
      }
    }

    return {}
  } catch (e: any) {
    console.error(e.message)
  }
}

export const getOrSave = async (name: string, callback: () => Promise<any> , ttl = 600) => {
  const cacheItem = await get(name)

  if (Object.keys(cacheItem).length > 0) {
    info(`Retrieved item "${name}" from cache`)

    return cacheItem.data
  } else {
    const data = await callback()

    if (data) {
      void save(name, data, ttl)
    } else {
      console.error("Did not save data to cache: Data is invalid.")
    }

    return data
  }
}

/**
 * Load the whole cache
 */
export const load = async () => {
  try {
    const cache = (await Browser.storage.local.get("cache")).cache
    return cache !== undefined ? cache : {}
  } catch (e: any) {
    console.error(e.message)
  }
}

/**
 * Save a value to the cache
 * @param name
 * @param data
 */
export const save = async (name: string, data: any, ttl = 600) => {
  try {
    const cache: CacheItem[] = await load()
    cache[name] = {
      data,
      timestamp: Date.now(),
      ttl
    }

    await Browser.storage.local.set({ cache })

    info(`Cache entry "${name}" has been saved for ${ttl} seconds.`)
  } catch (e: any) {
    console.error(e.message)
  }
}

/**
 *  Check if a cache object is still valid
 * @param item
 */
export const isValid = (item: CacheItem) => {
  try {
    return Date.now() <= item.timestamp + item.ttl * 1000
  } catch (e: any) {
    console.error(e.message)
  }
}

export const invalidate = async (name: string) => {
  try {
    const cache = (await load())
    delete cache[name]

    await Browser.storage.local.set({ cache })

    info(`Cache entry "${name}" has been invalidated`)
  } catch (e: any) {
    console.error(e.message)
  }
}
