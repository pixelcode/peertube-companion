import Browser from "webextension-polyfill"
import * as Invidious from "./api/invidious"
import * as Peertube from "./api/peertube"
import { info } from "./utils"
import * as Settings from "./settings"
import * as Cache from "./cache"
import { URL_TYPE } from "./backend"

export const disableRedirection = () => {
  Browser.webRequest.onBeforeRequest.removeListener(redirectionCallback)

  info("Redirection has been disabled")
}

export const enableRedirection = async () => {
  Browser.webRequest.onBeforeRequest.addListener(
    redirectionCallback, {
      urls: (await getHostsToRedirect()).hosts.map((url: string) => `*://${url}/*`),
      types: [ "main_frame", "xmlhttprequest" ]
    },
    [ "blocking" ]
  )

  info("Redirection has been enabled")
}

export const redirectionCallback = async (request: { url: string, type: string }) => {
  if (request.type === "xmlhttprequest") {
    const handledUrl = Peertube.handleLazyLoad(new URL(request.url)).href

    if (handledUrl !== request.url) {
      console.debug('b')
      console.debug(handledUrl, request.url)
      request.url = handledUrl
    } else {
      console.debug(handledUrl, request.url)
      return {}
    }
  }

  if ((await Cache.get('lastRedirectionUrl')).data !== request.url) { 
    // Do not redirect an URL which just has been redirected by the extension
    info("Handling redirection")
    const time = new Date().getTime()
    
    const video = await getRedirectionUrl(new URL(request.url))
    
    info(`Redirection handled in ${new Date().getTime() - time} ms`)
    
    if (video) {
      Cache.save('lastRedirectionUrl', video.url, await Settings.get('cache.ttl.redirection.last'))
      return {
        redirectUrl: video.url
      }
    }
  
    return {} // Do not redirect
  }
}

export const isValidHostToRedirect = async (url: URL, redirectPiped = true, redirectInvidious = true, redirectPeertube = true) => {
  return (await getHostsToRedirect(redirectPiped, redirectInvidious, redirectPeertube)).hosts.includes(url.host)
}

export const getHostsToRedirect = async (redirectPiped = true, redirectInvidious = true, redirectPeertube = true) => {
  return Cache.getOrSave(`getHostsToRedirect-${redirectPiped}-${redirectInvidious}-${redirectPeertube}`, async () => {
    let hosts = await Settings.get('api.youtube.redirectHosts')
    const types = []
  
    types[URL_TYPE.YOUTUBE] = hosts
  
    if (redirectInvidious) {
      const indexedInstances = await Invidious.getIndexedInstances(false)
      const mappedInstances = (indexedInstances).map((i: any[]) => i[0])
  
      hosts = indexedInstances ? hosts.concat(mappedInstances) : hosts
  
      types[URL_TYPE.INVIDIOUS] = mappedInstances
    }
  
    if (redirectPiped) {
      const pipedInstances = await Settings.get('api.piped.redirectHosts')
  
      hosts = hosts.concat(pipedInstances)
  
      types[URL_TYPE.PIPED] = pipedInstances
    }
  
    if (redirectPeertube) {
      const ptinstances = (await Peertube.getIndexedInstances()).map((o: any) => o.url)
  
      hosts = hosts.concat(ptinstances)
  
      types[URL_TYPE.PEERTUBE] = ptinstances
    }
  
    return { hosts, types }
  }, await Settings.get('cache.ttl.redirection.getHostsToRedirect'))
}

const getRedirectionUrl = async (url: URL) => {
  let rules = { rule: Peertube.MATCHING_RULES.NO_RULE, args: {} }

  switch (await getUrlType(url)) {
    case URL_TYPE.INVIDIOUS:
    case URL_TYPE.PIPED:
    case URL_TYPE.YOUTUBE: {
      // TODO Gérer plusieurs règles en même temps. Par exemple, le titre exact et regarder sur l'instance originale
      rules = { rule: Peertube.MATCHING_RULES.EXACT_TITLE, args: { title: await Invidious.getVideoTitle(url) } }
      
      break
    }

    case URL_TYPE.PEERTUBE: {
      rules = { rule: Peertube.MATCHING_RULES.ORIGINAL_INSTANCE, args: { url } }

      break
    }
    
    default:
      console.error("Unknown type.")
      return false
  }

  return (await Peertube.getVideoMatchingRules(rules)) || false
}

const getUrlType = async (url: URL) => {
  return Cache.getOrSave(`getUrlType-${encodeURIComponent(url.host)}`, async () => {
    for (const [ type, urls ] of (await getHostsToRedirect()).types.entries()) {
      if(urls.includes(url.host)) {
        return type
      }
    }

    return false
  }, await Settings.get('cache.ttl.redirection.getUrlBackedType'))
}

// const validateBlockedUrl = async (url: URL) => {
//   // https://tube.tr4sk.me/api/v1/search/videos?search=https://peertube.datagueule.tv/videos/watch/0b04f13d-1e18-4f1d-814e-4979aa7c9c44
// }
