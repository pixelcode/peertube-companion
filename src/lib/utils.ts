// import Browser from "webextension-polyfill"
import * as Settings from "./settings"
import get from "lodash.get"
import { config } from "../config"

export enum LOG {
  ERROR,
  INFO,
}

export enum PING_ERROR {
  ERROR,
  TIMEOUT,
  TYPE,
}

export enum BROWSER_MESSAGE {
  APP_DISABLE,
  APP_ENABLE,
  REDIRECTION_DISABLE,
  REDIRECTION_ENABLE,
  REDIRECTION_PEERTUBE_TOGGLE,
  INSTANCE_SELECTED_UPDATE
}

export interface InstanceInterface {
  url: string
  ping: number
}

/**
 * It fetches the data from the URL and returns the JSON response
 * @param {string} url - The URL to fetch from.
 * @returns The JSON data from the API
 */
export const fetchApi = async (url: string) => {
  try {
    info(`Fecthing URL ${url}`)

    const response = await fetch(url)

    return response ? response.json() : false
  } catch (e: any) {
    console.error(e.message)
    return false
  }

}

export const ping = async (url: URL, timeout = 3000, ignoreErrors = false) => {
  info(`Ping "${url.href}"`)
  const TIME_START = new Date().getTime()
  const controller = new AbortController()
  const signal = controller.signal
  const headers = new Headers()

  headers.append("pragma", "no-cache")
  headers.append("cache-control", "no-cache")

  const fetchPromise = fetch(url.href, { headers, signal })
  setTimeout(() => controller.abort(), timeout)

  return fetchPromise.then((r) => {
    if (!r.ok && !ignoreErrors) {
      throw new Error(`HTTP ${r.status} error while trying to fetch "${r.url}"`)
    }

    const t = new Date().getTime() - TIME_START

    info(`Pong "${url.href}": ${t}`)

    return t
  }).catch((e) => {
    console.error(e.message)
    return 0
  })
}

export const error = (message: string) => {
  log(message, LOG.ERROR)
}

export const info = (message: string) => {
  log(message, LOG.INFO)
}

export const log = (message: any, type?: LOG) => {
  const preMessage = "[PeerTube Companion] "
  switch (type) {
    case LOG.ERROR:
      if (config.app.log.error) {
        console.error(preMessage + message)
      }
      break
    case LOG.INFO:
      if (config.app.log.info) {
        console.info(preMessage + message)
      }
      break
    default:
      console.log(preMessage + message)
  }
}

export const sortByPing = (a: InstanceInterface, b: InstanceInterface) => {
  if (a.ping > b.ping) return 1
  if (a.ping < b.ping) return -1
  return 0
}

/**
 * Check if the app or a specific service is enabled
 * @param service
 */
export const isEnabled = async (service?: string) => {
  try {
    const settings = await Settings.load()

    if (service) {
      return get(settings, service)?.isEnabled || false
    }

    return settings.app.isEnabled
  } catch (e: any) {
    console.error(e.message)

    return false
  }
}

// export const toggleIcon = async () => {
//   if (await isEnabled()) {
//     await Browser.browserAction.setIcon({ path: new URL("../assets/icons/sepia.svg", import.meta.url).href })
//   } else {
//     await Browser.browserAction.setIcon({ path: new URL("../assets/icons/sepia-grayed.svg", import.meta.url).href })
//   }
// }
