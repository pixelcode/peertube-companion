export const config = {
  api: {
    invidious: {
      indexUrl: "https://api.invidious.io",
      isEnabled: true,
      preferredInstance: ""
    },
    peertube: {
      indexUrl: "https://instances.joinpeertube.org",
      searchUrl: "https://sepiasearch.org",
    },
    piped: {
      apiUrls: [
        "https://pipedapi.kavin.rocks/",
        "https://pipedapi.tokhmi.xyz/",
        "https://pipedapi.moomoo.me/",
        "https://pa.il.ax/",
        "https://pipedapi.syncpundit.com/",
        "https://api-piped.mha.fi/",
        "https://pipedapi.shimul.me/",
        "https://pa.mint.lgbt/",
        "https://piped-api.privacy.com.de/"
      ],
      isEnabled: true,
      redirectHosts: [
        "piped.kavin.rocks",
        "piped.tokhmi.xyz",
        "piped.moomoo.me",
        "il.ax",
        "piped.syncpundit.com",
        "piped.mha.fi",
        "piped.shimul.me",
        "piped.mint.lgbt",
        "piped.privacy.com.de"
  
      ],
      preferredInstance: ""
    },
    youtube: {
      redirectHosts: [
        "m.youtube.com",
        "youtube.com",
        "img.youtube.com",
        "www.youtube.com",
        "youtube-nocookie.com",
        "www.youtube-nocookie.com",
        "youtu.be",
        "s.ytimg.com"
      ]
    }
  },
  app: {
    isEnabled: true,
    log: {
      error: true,
      warning: true,
      info: true,
    }
  },
  cache: {
    ttl: {
      invidious: {
        pickInstance: 10 * 60, // 10 minutes
        getIndexedInstances: 1 * 3600, // 1 hour
        getVideoDetails: 1 * 24 * 3600 // 1 day
      },
      peertube: {
        getIndexConfig: 1 * 3600,
        getIndexedInstances: 1 / 2 * 24 * 3600, // Every 12 hours
        getInstanceConfig: 1 / 4 * 24 * 3600, // Every 6 hours
        getPerfectMatch: 1500, // in miliseconds
        getVideosDetails: 1 * 24 * 3600,
        searchByTitle: 1 / 2 * 3600 // 1/2 hour
      },
      redirection: {
        callback: 1 * 24 * 3600, // 1 day
        last: 5,
        getUrlBackedType: 1 * 24 * 3600,
        getHostsToRedirect: 1 * 24 * 3600
      }
    }
  },
  redirection: {
    preferredApi: "invidious",
    toOriginalInstance: false
  },
}
