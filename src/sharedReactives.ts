import { reactive } from "vue"

export const appState = reactive({
  isEnabled: true,
  invidious: {
    isEnabled: true
  },
  redirection: {
    toOriginalInstance: false
  }
})
