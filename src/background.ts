import * as Redirection from "./lib/redirection"
import { pickInstance } from "./lib/backend"
import Browser from "webextension-polyfill"
// import { BROWSER_MESSAGE, toggleIcon } from "./lib/utils"
import { BROWSER_MESSAGE } from "./lib/utils"

try {
  const run = () => {
    // await toggleIcon() // Ensure the icon reflects the app state
    void Redirection.enableRedirection()
    void pickInstance()

    Browser.runtime.onMessage.addListener(async (message) => {
      switch (message) {
        case BROWSER_MESSAGE.REDIRECTION_DISABLE:
        case BROWSER_MESSAGE.APP_DISABLE:
          Redirection.disableRedirection()

          break
        case BROWSER_MESSAGE.REDIRECTION_ENABLE:
        case BROWSER_MESSAGE.APP_ENABLE:
          await Redirection.enableRedirection()

          break
      }
    })
  }

  void run()
} catch (e: any) {
  console.error(e.message)
}
