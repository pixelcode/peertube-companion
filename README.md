PeerTube Companion is an experimental WebExtension aiming to improve user experience with PeerTube.

It's under heavy development and is considered unstable for now. Please, use it as your own risk.

Right now, the extension let you being redirected to a PeerTube version of a video, when trying to reach it from YouTube, if it's available on PeerTube.

More informations about future goals will be added once I have the time.

## Build from source
To build the webextension, you'll need to install yarn then use:
```console
yarn install
yarn build:prod
```

## Credits
This awesome Sepia illustration has been made by [David Revoy](https://www.davidrevoy.com/) for [PeerTube](https://joinpeertube.org/) project. It's licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.fr). 